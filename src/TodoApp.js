import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import rootReducer from './reducers';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import TodoForm from './components/TodoForm';

const store = createStore(rootReducer)

export default class TodoApp extends Component {

  render() {
    return (
        <Provider store={store}>
            <View style={styles.container}>
                <TodoForm/>
            </View>
        </Provider>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        padding: 5,
    }
});