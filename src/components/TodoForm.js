import React, { Component } from 'react';
import { View, StyleSheet, Button } from 'react-native';
import { connect } from 'react-redux';

import Input from './Input';
import { addTodo } from '../actions';

class TodoForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
        text: '',
    };
  }

  onChangeText(text) {
      this.setState({
          text
      })
  }

  onPress() {
      this.props.dispatchAddTodo(this.state.text);
  }

  render() {
    const { text } = this.state;
    return (
      <View style={styles.container}>
            <View style={styles.input}>
                <Input
                    onChangeText={ text => this.onChangeText(text)}
                    value={text}
                /> 
            </View>
            <View>
                <Button 
                    style={styles.button}
                    title="Salvar"
                    onPress={() => this.onPress()}/>
            </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    input: {
        flex: 3
    },
    button: {
        flex: 1
    }
});

// const mapDispatchToProps = {
//         ispatchAddTodo: text => dispatch(addTodo(text))
//         é o mesmo que a sintaxe abaixo
//         dispatchAddTodo: addTodo
// }

export default connect(null, {dispatchAddTodo: addTodo})(TodoForm);