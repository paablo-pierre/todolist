import React, { Component } from 'react';
import { View, TextInput, StyleSheet } from 'react-native';

export default class Input extends Component {

    render() {
        const { onChangeText, value } = this.props; 
        return (
            <TextInput
                style={styles.input}
                onChangeText={onChangeText}
                value={value}
            />
        );
    }
}

const styles = StyleSheet.create({
    input: {
        paddingLeft: 15,
        paddingRight: 15,
    }
});