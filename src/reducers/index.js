import { combineReducers } from 'redux';

import todoListReducer from './todoLIstReducer';

const rootReducer = combineReducers({
    todos: todoListReducer
});

export default rootReducer;